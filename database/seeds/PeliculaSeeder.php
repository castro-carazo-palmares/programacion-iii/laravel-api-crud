<?php

use Illuminate\Database\Seeder;

class PeliculaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peliculas')->insert([
            'nombre' => "Iron Man",
            'anio_lanzamiento' => "2008",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187412_396b76791a_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883961_6441620ed6_h.jpg',
            'descripcion' => "Tony Stark, dueño de Industrias Stark, inventor consagrado, vendedor de armas y playboy multimillonario, es secuestrado en Afganistán después de una demostración armamentística para el Ejército de los Estados Unidos. Forzado por sus captores a fabricar un arma temible, acaba construyendo en secreto una armadura de alta tecnología revolucionaria que usa para escaparse.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "The Incredible Hulk",
            'anio_lanzamiento' => "2008",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187312_fe0cf7b8b9_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688350608_ead7809c2a_h.jpg',
            'descripcion' => "El científico Bruce Banner continúa recorriendo el mundo en busca de un antídoto que haga frente al incontrolable poder que le dieron los rayos gamma y que le hace convertirse en Hulk cuando la furia reina se apodera de él.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Iron Man 2",
            'anio_lanzamiento' => "2010",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187397_ef0da2274b_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883941_600a35efe6_h.jpg',
            'descripcion' => "Después de los sucesos ocurridos en Iron Man, el mundo entero sabe que Tony Stark es el superhéroe enmascarado conocido como Iron Man. Tanto el gobierno, como la prensa y la opinión pública están haciendo campaña a favor de que Stark comparta su tecnología con el ejército, pero el creador de las Industrias Stark se niega a compartir sus secretos por el temor a que sus diseños caigan en manos de personas poco adecuadas y peligrosas.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Thor",
            'anio_lanzamiento' => "2011",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187522_77ee618287_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689187517_ed2f1172fd_h.jpg',
            'descripcion' => "Thor, príncipe del reino de Asgard, es un poderoso y arrogante guerrero cuya imprudencia desencadena antiguas rencillas en el dominio. Por ello, su padre Odín le envía al planeta Tierra a modo de castigo para que viva como un humano y descubra así el valor de la humildad. Cuando el malvado villano Loki, el más peligroso del universo, intenta invadir la Tierra, tan sólo una persona con fuerza sobrehumana podrá hacerle frente. Thor estará obligado a luchar y aprender así lo que significa ser un héroe de verdad. Todo ello se complica cuando su vida como hombre se ve ligada a la de una guapa científica, Jane Foster.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Captain America: The First Avenger",
            'anio_lanzamiento' => "2011",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350403_04eca6b891_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883606_90c3ba13de_h.jpg',
            'descripcion' => "Steve Rogers, endeble y tímido, quiere ser soldado y luchar por su país, pero por su físico es siempre rechazado hasta que se presenta como voluntario para participar en un programa experimental que va a transformarle en un super-soldado vengador con unas descomunales capacidades físicas. A partir de ese momento será conocido con el sobrenombre de Captain America y se convertirá en símbolo de esperanza y justicia para su nación.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "The Avengers",
            'anio_lanzamiento' => "2012",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350303_0558de1142_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689186972_659bd0f568_h.jpg',
            'descripcion' => "Cuando un enemigo inesperado amenaza la seguridad del planeta y de sus habitantes, Nick Fury, director de SHIELD, monta un dispositivo con todos los hombres capaces de preservar a la humanidad del caos. El enemigo es tan poderoso, que necesita que todos los superhéroes luchen juntos y formen un equipo compacto. Ellos serán: el Captain America, Thor, Iron Man, Hulk, Hawkeye y Black Widow.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Iron Man 3",
            'anio_lanzamiento' => "2013",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688883931_2c2b92b79f_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688350633_50b1010d09_h.jpg',
            'descripcion' => "En esta ocasión el imponente superhéroe tratará de frenar los pies de un nuevo villano: El Mandarín, quien hará trizas su universo personal y someterá a examen su valor y capacidad para cuidar de los suyos. Para llevar a cabo el que será uno de sus combates más difíciles, mejorará su armadura mediante una nueva tecnología y contará con la ayuda de su gran amiga Pepper Potts, su aliado de armas James Rhodes, y su guardaespaldas Happy Hogan.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Thor: The Dark World",
            'anio_lanzamiento' => "2013",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187507_7818ec7c92_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688350783_d6753d1d9f_h.jpg',
            'descripcion' => "Thor se ve obligado a embarcarse en su viaje más peligroso y al mismo tiempo personal, para enfrentarse a un enemigo que ni siquiera Odin y Asgard son capaces de vencer. La amenaza no es otra que una antigua raza liderada por el malvado Malekith, que quiere sumir al universo entero en la oscuridad.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Captain America: The Winter Soldier",
            'anio_lanzamiento' => "2014",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187127_d1264dd17e_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689187112_9a8723800e_h.jpg',
            'descripcion' => "Steve Rogers mantiene su alianza con Nick Fury y con la agencia secreta SHIELD, e intenta, además, involucrarse en el mundo moderno. Sin embargo, hay algo que Rogers no se imaginaba: un antiguo camarada suyo, conocido como el Soldado de Invierno, anteriormente Bucky Barnes, es encontrado por fuerzas enemigas en un lago congelado.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Guardians of the Galaxy",
            'anio_lanzamiento' => "2014",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187297_1bd44dfefe_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883831_e64243a7da_h.jpg',
            'descripcion' => "Un grupo de extravagantes ex-presidiarios formarán un equipo llamado los Guardianes de la Galaxia. Estos personajes son: Peter Quill, un piloto cazarrecompensas mitad alien-mitad humano; Rocket, un mapache armado con un rifle; Groot, un árbol humanoide; Gamora, una humana de color verde y Drax the Destroyer.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Avengers: Age of Ultron",
            'anio_lanzamiento' => "2015",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350073_09c9d3d72b_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688350063_bf9fdec2a6_h.jpg',
            'descripcion' => "El destino del planeta pende de un hilo cuando Tony Stark intenta hacer funcionar un inactivo programa para mantener la paz. Las cosas le salen mal y los héroes más poderosos se ven enfrentados a la prueba definitiva. Cuando el villano Ultrón aparece, es tarea de Los Vengadores el detenerle antes de que lleve a cabo sus terribles planes para el mundo.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Ant-Man",
            'anio_lanzamiento' => "2015",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688883256_78a1919c91_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689186662_1c731cc4d7_h.jpg',
            'descripcion' => "Tras abandonar la cárcel, el ladrón de medio pelo Scott Lang recibe la llamada del misterioso doctor Hank Pym para realizar un trabajo especial. El científico suministrará al joven un traje especial, que le otorgará la capacidad de reducir en gran medida su tamaño pero aumentando considerablemente su fuerza. Con esta nueva arma en su poder, deberá abrazar su héroe interior, olvidar su pasado de delincuente y tratar de salvar al mundo de una terrible amenaza.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Captain America: Civil War",
            'anio_lanzamiento' => "2016",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350378_3a872af5cb_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883566_06e4f4e7a5_h.jpg',
            'descripcion' => "Debido a los daños colaterales del trabajo de este grupo de superhéroes en anteriores misiones, la presión política busca depurar responsabilidades, y se instauran los Acuerdos de Sokovia. Es entonces cuando el gobierno de los Estados Unidos decide que los héroes necesitan ser supervisados y controlados. A partir de ahora, se exige a los Vengadores que respondan ante las Naciones Unidas, y se crea un consejo de administración que determinará cuándo van a necesitarse los servicios del equipo.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Doctor Strange",
            'anio_lanzamiento' => "2016",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688883731_22d2456521_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688350473_57ce871373_h.jpg',
            'descripcion' => "El doctor Stephen Strange es un reputado neurocirujano de Nueva York. Todo lo que tiene de brillante y talentoso también lo tiene de arrogante y vanidoso. Pero su vida no volverá a ser la misma después de que un terrible accidente de tráfico le prive del uso de las manos. Con sus manos dañadas, no puede ejercer su profesión y esto arruinará por completo su carrera.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Guardians of the Galaxy Vol. 2",
            'anio_lanzamiento' => "2017",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187257_8218486359_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883791_e34f0f35c1_h.jpg',
            'descripcion' => "El singular héroe Peter Quill, también conocido como Star-Lord, vuelve a embarcarse en un viaje intergaláctico por todo el universo. Le acompañará su heterogéneo equipo formado por Gamora, Drax el Destructor, Rocket Racoon y Baby Groot, además de otras nuevas incorporaciones como Mantis, Yondu Udonta y Nébula. Juntos, ahora que son una recién fundada familia, los Guardianes tendrán que luchar duro, a la vez que intentarán desentrañar los misterios que rodean el pasado de Star-Lord.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Spider-Man: Homecoming",
            'anio_lanzamiento' => "2017",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350733_3d1e26015b_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689187442_1232bd68d4_c.jpg',
            'descripcion' => "Peter Parker es un adolescente que estudia en la escuela secundaria mientras lidia con los problemas típicos de un chico de su edad. Aunque en realidad su día a día no es del todo normal. Y es que Peter esconde una identidad secreta, la de Spiderman. Así que tiene que compaginar su vida en el instituto con su labor de superhéroe que se enfrenta a todos aquellos villanos que ponen en peligro la ciudad, sin ser descubierto.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Thor: Ragnarok",
            'anio_lanzamiento' => "2017",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688884061_1e3a0f9c6f_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688884041_5de10c4db1_h.jpg',
            'descripcion' => "Asgard se encuentra en manos de una poderosa amenaza, la despiadada y todopoderosa Hela, que ha robado el trono y ha encarcelado a Thor, enviándole como prisionero hasta el otro extremo de la galaxia. Sin su martillo, el mítico y poderoso Mjölnir, el Dios del Trueno se encontrará a sí mismo en una carrera contra el tiempo.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Black Panther",
            'anio_lanzamiento' => "2018",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350348_1ee2d33e9a_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689187042_f5d1b53d7f_h.jpg',
            'descripcion' => "T'Challa regresa a su hogar en la apartada nación africana de Wakanda para servir como líder de su país. Tras suceder a su padre en el trono, pasa a convertirse en Pantera Negra, una sigilosa criatura de la noche, con agudos sentidos felinos y otras habilidades como súper fuerza e inteligencia, agilidad, estrategia o maestro del combate sin armas. Es bajo el liderazgo de T'Challa como Wakanda consigue convertirse en una de las naciones más ricas y tecnológicamente avanzadas del planeta.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Avengers: Infinity War",
            'anio_lanzamiento' => "2018",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688883411_6d3c7ab9a6_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689186882_28907b4795_h.jpg',
            'descripcion' => "Un nuevo peligro acecha procedente de las sombras del cosmos. Thanos, el infame tirano intergaláctico, tiene como objetivo reunir las seis Gemas del Infinito, artefactos de poder inimaginable, y usarlas para imponer su perversa voluntad a toda la existencia. Los Vengadores y sus aliados tendrán que luchar contra el mayor villano al que se han enfrentado nunca, y evitar que se haga con el control de la galaxia.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Ant-Man and the Wasp",
            'anio_lanzamiento' => "2018",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689186692_0f6f2388b0_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883266_1912a04a47_h.jpg',
            'descripcion' => "Scott Lang, más conocido como Ant-Man, ha tenido que aceptar las condiciones de los Acuerdos de Sokovia después de unirse al bando de Capitán América en la guerra conocida como Civil War. Además de lidiar con las consecuencias de sus acciones como superhéroe, Lang tiene deberes como padre e intentará equilibrar su vida familiar y sus responsabilidades como Ant-Man. Pero pronto Hope Van Dyne y Hank Pym volverán a llamar a su puerta para requerir su ayuda.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Captain Marvel",
            'anio_lanzamiento' => "2019",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688883691_2cf816b474_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689187137_4a4c47b975_h.jpg',
            'descripcion' => "Ambientada en los años 90, esta nueva aventura nos presenta un periodo de la historia nunca antes visto en el Universo Cinematográfico de Marvel. El viaje de Carol Danvers para convertirse en una de las heroínas más poderosas del universo, la Capitana Marvel, miembro de una raza de nobles héroes guerreros. Mientras una guerra galáctica entre dos razas alienígenas llega a la Tierra, Danvers luchará junto a un pequeño grupo de aliados, mientras trata de encontrarse a sí misma para descubrir quién es en realidad.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Avengers: Endgame",
            'anio_lanzamiento' => "2019",
            'genero_id' =>  DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49688350138_0386d6dcb6_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49689186822_0d8ed2ed3d_h.jpg',
            'descripcion' => "Después de los devastadores eventos ocurridos en Vengadores: Infinity War, el universo está en ruinas debido a las acciones de Thanos, el Titán Loco. Tras la derrota, las cosas no pintan bien para los Vengadores. El grupo deberá tratar de revertir los efectos de la catástrofe provocada por Thanos. Juntos, se prepararán para la batalla final, sin importar cuáles sean las consecuencias.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('peliculas')->insert([
            'nombre' => "Spider-Man: Far from home",
            'anio_lanzamiento' => "2019",
            'genero_id' => DB::table('generos')->where('nombre', 'Acción')->pluck('id')->first(),
            'imagen_sd' => 'https://live.staticflickr.com/65535/49689187432_5edafe42d4_c.jpg',
            'imagen_hd' => 'https://live.staticflickr.com/65535/49688883986_f3777866d9_h.jpg',
            'descripcion' => "Peter Parker decide dejar sus heroicidades a un lado durante unas pocas semanas, y sale de viaje por Europa con sus amigos Ned, MJ y el resto de compañeros de instituto. Pero las vacaciones de verano darán un giro inesperado cuando aparezca Nick Fury, que tiene trabajo para Peter. Los Elementales, criaturas de arena, piedra, agua y fuego, están creando el caos y la destrucción en todo el continente.",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
