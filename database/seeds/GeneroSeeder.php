<?php

use Illuminate\Database\Seeder;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->insert(['nombre' => "Horror"]);
        DB::table('generos')->insert(['nombre' => "Romance"]);
        DB::table('generos')->insert(['nombre' => "Acción"]);
        DB::table('generos')->insert(['nombre' => "Sci-Fi"]);
        DB::table('generos')->insert(['nombre' => "Documental"]);
        DB::table('generos')->insert(['nombre' => "Aventura"]);
        DB::table('generos')->insert(['nombre' => "Deportes"]);
        DB::table('generos')->insert(['nombre' => "Comedia"]);
        DB::table('generos')->insert(['nombre' => "Crimen"]);
        DB::table('generos')->insert(['nombre' => "Misterio"]);
        DB::table('generos')->insert(['nombre' => "Drama"]);
        DB::table('generos')->insert(['nombre' => "Fantasía"]);
        DB::table('generos')->insert(['nombre' => "Musical"]);
        DB::table('generos')->insert(['nombre' => "Guerra"]);
        DB::table('generos')->insert(['nombre' => "Familiar"]);
        DB::table('generos')->insert(['nombre' => "Biografía"]);
    }
}
