<?php

namespace App\Http\Controllers;

use App\Pelicula;
use Illuminate\Http\Request;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pelicula::with('generos')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'nombre' => 'required',
            'anio_lanzamiento' => 'required',
            'genero_id' => 'required',
            'descripcion' => 'required',
            'imagen_sd' => 'required',
            'imagen_hd' => 'required'
        ]);

        $pelicula = Pelicula::create($fields);

        return response()->json($pelicula, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function show(Pelicula $pelicula)
    {
        return $pelicula;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelicula $pelicula)
    {
        $fields = $request->validate([
            'nombre' => 'required',
            'anio_lanzamiento' => 'required',
            'genero_id' => 'required',
            'descripcion' => 'required',
            'imagen_sd' => 'required',
            'imagen_hd' => 'required'
        ]);

        $pelicula->update($fields);

        return response()->json($pelicula, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pelicula  $pelicula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelicula $pelicula)
    {
        $pelicula->delete();

        return response()->json($pelicula, 200);
    }
}
