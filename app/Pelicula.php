<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $fillable = ['nombre', 'anio_lanzamiento', 'genero_id', 'imagen_sd', 'imagen_hd', 'descripcion'];

    public function generos()
    {
        return $this->belongsTo('App\Genero', 'genero_id');
    }
}
